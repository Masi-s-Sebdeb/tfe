from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics, authentication, permissions
from .serializers import  UserSerializer, AuthTokenSerializer
from rest_framework.settings import api_settings
from rest_framework.authtoken.views import ObtainAuthToken





from .permissions import IsAdminUser
from django.http import JsonResponse
import os
import base64
import random
import numpy as np
import tensorflow as tf
from tensorflow import keras
import json
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import preprocessing as pp

class GetProjectList(APIView):
    def get(self, request, *args, **kwargs):
        project_list = os.listdir('saved_model')
        return Response({"project_list": project_list}, status=200)


class CreateUserView(generics.CreateAPIView):
    """Create a new user in the system"""
    serializer_class = UserSerializer


class CreateTokenView(ObtainAuthToken):
    """Create a new auth token for user"""
    serializer_class = AuthTokenSerializer
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class ManageUserView(generics.RetrieveUpdateAPIView):
    """Manage the authenticated user"""
    serializer_class = UserSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self):
        """Retrieve and return authentication user"""
        return self.request.user
    

class UploadProject(APIView):
    
    def post(self,request,*args,**kwargs):
        project_name = request.POST.get('project_name')
        project_zip = request.FILES.get('file')
        if not project_zip :
            return JsonResponse({"error": "No file found"}, status=400)
        if project_zip and project_zip.name.endswith('.zip'):
            os.system('mkdir saved_model/{}'.format(project_name))
            file_path = 'saved_model/{}/{}'.format(project_name, project_zip.name)
            with open(file_path, 'wb') as f:
                for chunk in project_zip.chunks():
                    f.write(chunk)
            os.system('unzip {} -d saved_model/{}'.format(file_path, project_name))
            os.system('rm {}'.format(file_path))
            return JsonResponse({"success": "Project uploaded successfully"}, status=200)

class DeleteProject(APIView):
    
    def post(self, request, *args, **kwargs):
        project_name = request.POST.get('project_name')
        os.system('rm -rf saved_model/'+project_name)
        return JsonResponse({"success": "Project deleted successfully"}, status=200)
            
class FileUploadView(APIView):
    parser_classes = [MultiPartParser]

    def post(self, request, *args, **kwargs):
        uploaded_file = request.FILES.get('file')
        project_name = request.POST.get('project_name')
        delimiter = request.POST.get('delimiter')
        if not uploaded_file:
            print("Request.FILES: ", request.FILES)
            return JsonResponse({"error": "No file found"}, status=400)
        if uploaded_file and (uploaded_file.name.endswith('.txt') or uploaded_file.name.endswith('.csv')):
	    
            file_content = uploaded_file.read().decode('utf-8')
            X_test = np.array([np.array([float(x) for x in line.split(delimiter)]) for line in file_content.split('\n') if line != ''])
            dcam, permut, pred = self.array_to_dCAM(X_test,project_name)
            dims = self.get_dims(project_name)
            plt.figure(figsize=(24, 8))
            sns.heatmap(pp.MinMaxScaler().fit_transform(dcam),cmap='viridis',yticklabels=dims)
            plt.savefig('dcam.png')
            plt.close()
            

            means = np.mean(pp.MinMaxScaler().fit_transform(dcam), axis=1)
            maxs_top3_indices = [0,0,0]
            for i in range(len(means)):
                if means[i] > means[maxs_top3_indices[0]]:
                    maxs_top3_indices[0] = i
            for i in range(len(means)):
                if means[i] > means[maxs_top3_indices[1]] and i != maxs_top3_indices[0]:
                    maxs_top3_indices[1] = i
            for i in range(len(means)):
                if means[i] > means[maxs_top3_indices[2]] and i != maxs_top3_indices[0] and i != maxs_top3_indices[1]:
                    maxs_top3_indices[2] = i
            

            all_classes_std_mean = np.load('saved_model/'+project_name+'/all_classes_std_mean_0.npy')
            all_classes_std_mean_1 = np.load('saved_model/'+project_name+'/all_classes_std_mean_1.npy')
            
            figs = []
            for j in range(len(all_classes_std_mean)):
                plt.figure(figsize=(30,5))
                plt.fill_between(range(len( all_classes_std_mean[j][1])), all_classes_std_mean[j][1]-all_classes_std_mean[j][0], all_classes_std_mean[j][1]+all_classes_std_mean[j][0], alpha=0.5, color='teal')
                plt.fill_between(range(len( all_classes_std_mean[j][1])), all_classes_std_mean_1[j][1]-all_classes_std_mean_1[j][0], all_classes_std_mean_1[j][1]+all_classes_std_mean_1[j][0], alpha=0.5, color='salmon')
                plt.plot(X_test.T[j], color='black')
                plt.legend([ 'Standard deviation Normal - 0', 'Standard deviation Anomaly - 1','Input'])
                plt.title('Dimension '+str(j))
                plt.ylabel(dims[j])
                plt.savefig('test.png')
                plt.close()
                with open('test.png', 'rb') as f:
                    figs.append(base64.b64encode(f.read()).decode('utf-8'))
                    

            with open('dcam.png', 'rb') as f:
                dcam_data = base64.b64encode(f.read()).decode('utf-8')
            
            
            return JsonResponse({'dcam' : json.dumps(maxs_top3_indices),'dcam_png':dcam_data ,'permutation' : permut, 'prediction' : json.dumps(pred.tolist()), 'dims' : json.dumps(dims), 'figs' : json.dumps(figs)}, status=200)
        else:
            return JsonResponse({"error": "File type not supported"}, status=400)
	
    def gen_cube(self,instance):
        result = []
        for i in range(len(instance)):
            result.append([instance[(i+j)%len(instance)] for j in range(len(instance))])
        return result
    def numpy_to_tf_input(self,numpy_input):
        # Transpose the array to move the channel dimension to the last axis
        transposed_input = np.transpose(numpy_input, (0, 2, 3, 1))
        return transposed_input
    def get_dims(self,project_name):
        with open('saved_model/'+project_name+'/config_file.txt', 'r') as f:
            dims = f.readlines()
            for i in range(len(dims)):
                line = dims[i].split(':')
                if line[0] == 'dimensions_list':
                    return line[1].split(',')
        return ['nodims']
          
    def array_to_dCAM(self,array,project_name):
    
        model = keras.models.load_model('saved_model/'+project_name)
        T = array.transpose(1,0)
        cube = self.gen_cube(T)
        cube = np.array(cube)
        cube = cube.reshape(1,cube.shape[0], cube.shape[1], cube.shape[2])

        prediction = model.predict(self.numpy_to_tf_input(cube))
        prediction = prediction.argmax(axis=-1)

        dcam = DCAM(model,last_conv_layer='last-conv')
        heatmap, permutation = dcam.run(
			instance=T,
			nb_permutation=200,
			label_instance=prediction
		)

        return heatmap,permutation,prediction
class DCAM():
	def __init__(self,model,last_conv_layer='layer3'):
		self.last_conv_layer = last_conv_layer
		self.model = model
		
	def run(self,instance,nb_permutation,label_instance):
		all_permut,permut_success = self.__compute_permutations(instance, nb_permutation,label_instance)
		dcam =  self.__extract_dcam(self.__merge_permutation(all_permut))
		return dcam,permut_success

	def __gen_cube_random(self,instance):
		result = []
		result_comb = []
		initial_comb = list(range(len(instance)))
		random.shuffle(initial_comb)
		for i in range(len(instance)):
			result.append([instance[initial_comb[(i+j)%len(instance)]] for j in range(len(instance))])
			result_comb.append([initial_comb[(i+j)%len(instance)] for j in range(len(instance))]) 
		
		return result,result_comb


	def __merge_permutation(self,all_matfull_list):
		full_mat_avg = np.zeros((len(all_matfull_list[0]),len(all_matfull_list[0][0]),len(all_matfull_list[0][0][0])))
		for i in range(len(all_matfull_list[0])):
			for j in range(len(all_matfull_list[0][0])):
				mean_line = np.array([np.mean([all_matfull_list[k][i][j][n] for k in range(len(all_matfull_list))]) for n in range(len(all_matfull_list[0][0][0]))])
				full_mat_avg[i][j] = mean_line
		return full_mat_avg

	def __extract_dcam(self,full_mat_avg):
		return np.mean((full_mat_avg-np.mean(full_mat_avg,1))**2,1)*np.mean(np.mean(full_mat_avg,1),0)

	def __make_gradcam_heatmap(self,img_array, pred_index=None):
    
		img_array = np.array(img_array)
		img_array = img_array.reshape((1,)+img_array.shape)
		img_array = np.transpose(img_array, (0, 2, 3, 1))
		predict = self.model.predict(img_array)
		predict = np.argmax(predict)
		
		grad_model = tf.keras.models.Model(
			[self.model.inputs], [self.model.get_layer(self.last_conv_layer).output, self.model.output]
		)

		# Then, we compute the gradient of the top predicted class for our input image
		# with respect to the activations of the last conv layer
		with tf.GradientTape() as tape:
			last_conv_layer_output, preds = grad_model(img_array)
			if pred_index is None:
				pred_index = tf.argmax(preds[0])
			class_channel = preds[:, pred_index]

		# This is the gradient of the output neuron (top predicted or chosen)
		# with regard to the output feature map of the last conv layer
		grads = tape.gradient(class_channel, last_conv_layer_output)

		# This is a vector where each entry is the mean intensity of the gradient
		# over a specific feature map channel
		pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))

		# We multiply each channel in the feature map array
		# by "how important this channel is" with regard to the top predicted class
		# then sum all the channels to obtain the heatmap class activation
		last_conv_layer_output = last_conv_layer_output[0]
		heatmap = last_conv_layer_output @ pooled_grads[..., tf.newaxis]
		heatmap = tf.squeeze(heatmap)
		
		# For visualization purpose, we will also normalize the heatmap between 0 & 1
		#heatmap = tf.maximum(heatmap, 0) / tf.math.reduce_max(heatmap)
		return heatmap.numpy(),predict

	def __compute_multidim_cam(self,instance,nb_dim,index_perm):
		acl,comb = self.__gen_cube_random(instance)
		overlay,pred_class = self.__make_gradcam_heatmap(acl)
		full_mat = np.zeros((nb_dim,nb_dim,len(overlay[0])))
		for i in range(nb_dim):
			for j in range(nb_dim):
				full_mat[comb[i][j]][i] = overlay[j]
		return overlay,full_mat,pred_class


	def __compute_permutations(self,instance, nb_permutation,label_instance):
		all_pred_class = []
		all_matfull_list = []

		final_mat = np.zeros((len(instance),len(instance)))
		for k in range(0,nb_permutation):
			_,fmat,class_pred = self.__compute_multidim_cam(instance,len(instance),k)
			if class_pred == label_instance :
				all_matfull_list.append(fmat)
			all_pred_class.append(class_pred)
		if np.std(all_pred_class) == 0:
			return all_matfull_list,nb_permutation
		else:
			#verbose
			#print("[WARNING]: misclassification for some permutations:")
			#print("|--> Please note that every misclassified permutations will not be taken into account")
			#print("|--> The total number of permutation used to compute DCAM is lower than the number given as parameter")
			#print("|--> Number correctly classified permutations: {}".format(all_pred_class.count(label_instance)))
			#print("|--> Percentage of correctly classified permutations: {}".format(float(all_pred_class.count(label_instance))/float(len(all_pred_class))))
			return all_matfull_list,float(all_pred_class.count(label_instance))
		
