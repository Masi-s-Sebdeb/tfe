from django import urls
from django.urls import path, include
from .views import FileUploadView, GetProjectList, UploadProject, DeleteProject,CreateTokenView,ManageUserView,CreateUserView


urlpatterns = [
    path('upload/', FileUploadView.as_view(), name='file_upload'),
    path('project_list/', GetProjectList.as_view(), name='project_list'),
    path('project_upload/', UploadProject.as_view(), name='project_upload'),
    path('project_delete/', DeleteProject.as_view(), name='project_delete'),
    path('create/', CreateUserView.as_view(), name='create'),
    path('token/', CreateTokenView.as_view(), name='token'),
    path('me/', ManageUserView.as_view(), name='me'),

]