import React, { useState }  from "react";

import './App.css';
import Uploader from './components/Uploader.component';
import { FundOutlined } from "@ant-design/icons";
import Home from "./components/Home.component";
import Vis from "./components/Vis.component";
import Admin from "./components/Admin.component";


function App() {
  const [activeOption, setActiveOption] = useState(0);
  const [page, setPage] = useState(<Home />);
  const handleOptionClick = (optionIndex) => {
    setActiveOption(optionIndex);
    if (optionIndex === 0) {
      setPage(<Home />);
    }
    if (optionIndex === 1) {
      setPage(<Vis />);
    }
    if (optionIndex === 2) {
      setPage(<Admin />);
    }
    
  };


  return (
    <div className="App">
      <div style={{ backgroundColor: '#112d4e', display: 'flex', justifyContent: 'space-between', padding: '20px',paddingLeft: '100px', paddingRight: '100px' }}>
        <div style={{}}> 
          <FundOutlined style={{ fontSize: '25px', color: 'white' }} />
          <h5 style={{ color: 'white',fontSize: '20px' }}>dCAM</h5>
        </div>
        

        <div style={{ display: 'flex', justifyContent: 'space-between', maxWidth: '600px', width: '100%',margin: '100x 0px' }}>
          <NavbarOption
            title="Home"
            index={0}
            activeOption={activeOption}
            handleClick={handleOptionClick}
          />
          <NavbarOption
            title="Explanation"
            index={1}
            activeOption={activeOption}
            handleClick={handleOptionClick}
          />
          <NavbarOption
            title="Admin"
            index={2}
            activeOption={activeOption}
            handleClick={handleOptionClick}
          />
        </div>
        
        <div style={{}}>
          <p></p>
        </div>
      </div>
      {page}
    </div>
  );
}

const NavbarOption = ({ title, index, activeOption, handleClick }) => {
  const isActive = index === activeOption;

  return (
    <div
      style={{
        color: isActive ? 'white' : 'rgba(255, 255, 255, 0.7)',
        borderBottom: isActive ? '3px ' : 'none',
        cursor: 'pointer',
        padding: '10px',
        textAlign: 'center',
        width: '33.33%',
        position: 'relative',
        fontSize: '24px'
      }}
      onClick={() => handleClick(index)}
    >
      {title}
      {isActive && (
        <div
          style={{
            backgroundColor: 'white',
            height: '2px',
            left: '50%',
            position: 'absolute',
            transform: 'translateX(-50%)',
            width: '10%'  // Change this to adjust the width of the underline
          }}
        />
      )}
    </div>
  );
};


export default App;
