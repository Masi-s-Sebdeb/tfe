
import React, { useState } from 'react';

const Navbar = () => {
  const [activeOption, setActiveOption] = useState(0);

  const handleOptionClick = (optionIndex) => {
    setActiveOption(optionIndex);
  };

  return (
    <div style={{ backgroundColor: '#112d4e', display: 'flex', justifyContent: 'center' }}>
      <div style={{ display: 'flex', justifyContent: 'space-between', maxWidth: '600px', width: '100%' }}>
        <NavbarOption
          title="Option 1"
          index={0}
          activeOption={activeOption}
          handleClick={handleOptionClick}
        />
        <NavbarOption
          title="Option 2"
          index={1}
          activeOption={activeOption}
          handleClick={handleOptionClick}
        />
        <NavbarOption
          title="Option 3"
          index={2}
          activeOption={activeOption}
          handleClick={handleOptionClick}
        />
      </div>
    </div>
  );
};

const NavbarOption = ({ title, index, activeOption, handleClick }) => {
  const isActive = index === activeOption;

  return (
    <div
      style={{
        color: isActive ? 'white' : 'rgba(255, 255, 255, 0.7)',
        borderBottom: isActive ? '2px solid white' : 'none',
        cursor: 'pointer',
        padding: '10px',
        textAlign: 'center',
        width: '33.33%'
      }}
      onClick={() => handleClick(index)}
    >
      {title}
    </div>
  );
};

export default Navbar;