import React, { useState,useEffect,useRef  } from 'react';
import { AutoComplete } from 'antd';


import {CheckOutlined } from '@ant-design/icons';
const Vis = () => {
    const [dcamPng, setDcamPng] = useState('');
    const [pred, setPred] = useState(-1);
    const [dims3, setDims3] = useState(['','','']);
    const [allDims, setAllDims] = useState([]);
    const [selectedDim, setSelectedDim] = useState([]);
    const [figs, setFigs] = useState([]);
    useEffect(() => {
        const storedVariable = localStorage.getItem('dcam');
        const storedVariablePng = localStorage.getItem('dcam_png');
        const storedVariableDims = localStorage.getItem('dims');
        const storedVariablePred = localStorage.getItem('pred');
        const storedVariableFigs = localStorage.getItem('figs');

        const data = JSON.parse(storedVariable);
        const dims = JSON.parse(storedVariableDims);
        let f = JSON.parse(storedVariableFigs);
        if (storedVariable) {

        let str = dims.slice(1,-1);
        let table= str.split(',');
        let table1 = table[0].slice(1,-1);
        table = table.slice(1).map((item) => item.slice(2,-1));
        table.unshift(table1);
        

        setPred(storedVariablePred.slice(2,-2));
        setAllDims(table);
        console.log(table);
        setSelectedDim(table[data[0]]);
        setDims3([table[data[0]],table[data[1]],table[data[2]]]);
        setDcamPng(storedVariablePng);
        
        f = f.slice(1,-1);
        f = f.split(',');
        let f1 = f[0].slice(1,-1);
        f = f.slice(1).map((item) => item.slice(2,-1));
        f.unshift(f1);
        setFigs(f);
        console.log(f);
        }
    }, []);
    
    const handleSelect = (value) => {
        console.log(value);
        setSelectedDim(value);
    }
    return (

        <div>
            <div style={{  display: 'flex', justifyContent: 'space-between', padding: '20px',paddingLeft: '100px',marginTop:'-20px'}}>
                <div style={{'display': 'flex', 'justifyContent': 'start', 'maxWidth': '600px', 'width': '100%','alignItems': 'flex-start','flexDirection': 'column','marginTop':'50px'}}>
                    <h1>
                    This sample is predicted as <span style={{'display':'inline-block','border':"2px solid black",'borderRadius':'50%','padding':'5px 10px'}}>{pred}</span>
                    </h1>
                    <p style={{'fontSize': '20px','display': 'flex', 'justifyContent': 'space-between','flexDirection': 'column','alignItems': 'flex-start'}}>
                        <p style={{'textAlign':'left'}}>It seems that the three most important dimensions<br></br> to determine the prediction are</p> 
                        <p><CheckOutlined style={{ color: 'green' }}/> {dims3[0]}</p>
                        <p><CheckOutlined style={{ color: 'green' }} /> {dims3[1]}</p>
                        <p><CheckOutlined style={{ color: 'green' }} /> {dims3[2]}</p>
                    </p>
                    <div style={{ 'display':'flex','flexDirection':'row','justifyContent':'center','marginLeft':'100px'}}>
                        <h3>Dimensions : </h3> 
                        <AutoComplete
                            options={allDims.map((option) => ({ value: option }))}
                            onSelect={handleSelect}
                            placeholder="Select an option"
                            value={selectedDim}
                            style={{ width: 200,marginLeft:'10px',marginTop:'-5px' }}
                        />
                    </div>
                </div>
                <div style={{'overflow': 'hidden','marginLeft': '-100px'}}>
                    <img src={`data:image/png;base64,${dcamPng}`} alt="DCAM" style={{'width': '115%'}} />
                </div>
            </div>
            <div>
                
                <img src={`data:image/png;base64,${figs[allDims.findIndex((item) => item === selectedDim)]}`} alt="DCAM" style={{'width': '100%'}} />
            </div>
        </div>
    )

}

  
export default Vis
