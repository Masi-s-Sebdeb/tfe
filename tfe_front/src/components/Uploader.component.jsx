import React, { useState } from 'react';
import { Upload, Button, message } from 'antd';
import { UploadOutlined } from '@ant-design/icons';


const Uploader= (props) => {
    const [fileList, setFileList] = useState([]);

    const handleChange = (info) => {
        const newFileList = info.fileList.map((file) => file.originFileObj);
        setFileList(newFileList);
      };
  
      const handleUpload = async () => {
        const formData = new FormData();
        fileList.forEach((file) => {
          formData.append('file', file);
          formData.append('project_name', props.project_name);
          //formData.append('delimiter', props.delimiter);
        });
      
        try {
          const response = await fetch('http://localhost:8000/api/upload/', {
            method: 'POST',
            body: formData,
          });
      
          if (response.ok) {
            const data = await response.json();
            const dcam = JSON.parse(data.dcam);
            const dcamArray = [];
            for (let i = 0; i < dcam.length; i++) {
              const row = [];
              for (let j = 0; j < dcam[i].length; j++) {
                row.push(dcam[i][j]);
              }
              dcamArray.push(row);
            }
            console.log(dcamArray);
            localStorage.setItem('dcam', JSON.stringify(dcamArray));
            console.log(data.permutation);
            console.log(data.prediction);
            message.success('File uploaded successfully');
          } else {
            message.error('File upload failed');
          }
        } catch (error) {
          console.error(error);
        }
      };
      
    const uploadProps = {
      onRemove: (file) => {
        setFileList((prevFileList) => {
          return prevFileList.filter((item) => item.uid !== file.uid);
        });
      },
      beforeUpload: (file) => {
        setFileList([...fileList, file]);
        return false;
      },
      fileList,
      multiple: false,
    };
  
    return (
      <div>
        <Upload {...uploadProps}>
          <Button icon={<UploadOutlined />}>Select a text file</Button>
        </Upload>
        <Button
          type="primary"
          onClick={handleUpload}
          disabled={fileList.length === 0}
          style={{ marginTop: 16 }}
        >
          Upload
        </Button>
      </div>
    );
  };
  
  export default Uploader;

  
  
  
  