import React, { useState,useEffect } from 'react';
import Uploader from './Uploader.component';
import { Select,Button,Input,Upload,message} from 'antd';
import { UploadOutlined } from '@ant-design/icons';

const { Option } = Select;
const Home = () => {
    const [options, setOptions] = useState(['']);
    const [selectedOption, setSelectedOption] = useState('');
     const [delimiter, setDelimiter] = useState(',');
    const handleSelectChange = (event) => {
        console.log(event);
        setSelectedOption(event);
        
      };
      
      useEffect(() => {
        const fetchProjects = async () => {
            fetch('http://localhost:8000/api/project_list/')
            .then(response => response.json())
            .then(data => {
                setOptions(data.project_list);
            })
            .catch(error => console.error(error));
          
        };
    
        fetchProjects();
      }, []);
      const [fileList, setFileList] = useState([]);

    const handleChange = (info) => {
        const newFileList = info.fileList.map((file) => file.originFileObj);
        setFileList(newFileList);
      };
  
      const handleUpload = async () => {
        const formData = new FormData();
        fileList.forEach((file) => {
          formData.append('file', file);
          formData.append('project_name', selectedOption);
          formData.append('delimiter', delimiter);
        });
      
        try {
          const response = await fetch('http://localhost:8000/api/upload/', {
            method: 'POST',
            body: formData,
          });
      
          if (response.ok) {
            const data = await response.json();
            const dcam = JSON.parse(data.dcam);
            const dcam_png = data.dcam_png;
            localStorage.setItem('dcam_png', dcam_png);
            localStorage.setItem('dims', JSON.stringify(data.dims));
            localStorage.setItem('dcam', JSON.stringify(dcam));
            localStorage.setItem('pred', JSON.stringify(data.prediction));
            localStorage.setItem('figs', JSON.stringify(data.figs));
            message.success('File uploaded successfully');
          } else {
            message.error('File upload failed');
          }
        } catch (error) {
          console.error(error);
        }
      };
      
    const uploadProps = {
      onRemove: (file) => {
        setFileList((prevFileList) => {
          return prevFileList.filter((item) => item.uid !== file.uid);
        });
      },
      beforeUpload: (file) => {
        setFileList([...fileList, file]);
        return false;
      },
      fileList,
      multiple: false,
    };
    
    return (
        <div style={{'display':"flex", 'flex-direction': 'row','padding': '80px','paddingLeft': '100px', 'paddingRight': '200px','marginTop':'30px'}}>
            <div style={{'width':'50%'}}>
                <p style={{'font-size': '25px', 'text-align': 'left', 'margin': '20px'}}>The purpose of this platerforme is to generate dimension-wise activation map for multivariate time series. To generate a dCAM you just have to upload the data and be sure an admin deployed your model !</p>
                <div style={{'display':'flex','flex-direction':'column','margin':'20px',marginTop:'80px'}}>
                    <div style={{'display':'flex','flex-direction':'row'}}>
                        <h1>1. Upload Data : </h1>
                        <Upload {...uploadProps}>
                            <Button style={{'marginLeft':'50px'}} icon={<UploadOutlined />}>Select a text file</Button>
                        </Upload>
                        <p style={{'marginLeft':'10px','font-size':'15px','marginTop':'10px'}}>(.csv,.txt)</p>
                    </div>
                    <div style={{'display':'flex','flex-direction':'row'}}>
                        <h1>2. Delimiter :</h1>
                        <Input  placeholder="Delimiter" value={delimiter} onChange={(e) => setDelimiter(e.target.value)} style={{'height':'20%','width':'30%','margin':'10px',marginLeft:'100px'}}  />
                        
                    </div>
                    <div style={{'display':'flex','flex-direction':'row'}}>
                        <h1>3. Project :</h1>
                        <Select style={{'margin':'10px',marginLeft:'130px',}} value={selectedOption} onChange={handleSelectChange}>
                            <Option value="">Select an option</Option>
                            {options.map((option, index) => (
                            <Option key={index} value={option}>
                                {option}
                            </Option>
                            ))}
                        </Select>
                    </div>
                </div>
                <Button
                    type="primary"
                    onClick={handleUpload}
                    disabled={fileList.length === 0}
                    style={{ marginTop: 16 ,backgroundColor: '#112d4e',width: '200px'}}   
                    >
                <p style={{color: 'white'}}>Upload</p>
                </Button>

            </div>
            <div style={{"width":"50%","marginLeft":'100px','marginTop':'20px'}}>
                  <img src="front-format.png" style={{'width':'100%'}}/>           
            </div>
        </div>
    )
}

export default Home
