import React, { useState,useEffect } from 'react';
import Uploader from './Uploader.component';
import { Select,Button,Input,Upload,message, List } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

const { Option } = Select;
const Admin = () => {
    const [options, setOptions] = useState(['']);
    const [selectedOption, setSelectedOption] = useState('');
     const [delimiter, setDelimiter] = useState('Project name');
    const handleSelectChange = (event) => {
        console.log(event);
        setSelectedOption(event);
        
      };
      
      useEffect(() => {
        const fetchProjects = async () => {
            fetch('http://localhost:8000/api/project_list/')
            .then(response => response.json())
            .then(data => {
                setOptions(data.project_list);
            })
            .catch(error => console.error(error));
          
        };
    
        fetchProjects();
      }, []);
      const [fileList, setFileList] = useState([]);

    const handleDelete = async (item) => {
        console.log(item);
        const formData = new FormData();
        formData.append('project_name', item);
        try {
            const response = await fetch('http://localhost:8000/api/project_delete/', {
                method: 'POST',
                body: formData,
            });
            if (response.ok) {
                message.success('Project deleted successfully');
            } else {
                message.error('Project deletion failed');
            }
        } catch (error) {
            console.error(error);
        }
    };

    const handleChange = (info) => {
        const newFileList = info.fileList.map((file) => file.originFileObj);
        setFileList(newFileList);
      };
  
      const handleUpload = async () => {
        const formData = new FormData();
        fileList.forEach((file) => {
          formData.append('project_name', selectedOption);
          formData.append('file', file);
          console.log(file);
          console.log(selectedOption);

        });
      
        try {
          const response = await fetch('http://localhost:8000/api/project_upload/', {
            method: 'POST',
            body: formData,
          });
      
          if (response.ok) {
            console.log('ok');
          } else {
            message.error('File upload failed');
          }
        } catch (error) {
          console.error(error);
        }
      };
    
    const uploadProps = {
      onRemove: (file) => {
        setFileList((prevFileList) => {
          return prevFileList.filter((item) => item.uid !== file.uid);
        });
      },
      beforeUpload: (file) => {
        setFileList([...fileList, file]);
        return false;
      },
      fileList,
      multiple: false,
    };
    
    return (
        <div style={{'display':"flex", 'flex-direction': 'row','padding': '80px','paddingLeft': '100px', 'paddingRight': '200px','marginTop':'30px'}}>
            <div style={{'width':'50%'}}>
                <p style={{'font-size': '25px', 'text-align': 'left', 'margin': '2px','marginBottom':'70px'}}>The purpose of this platerforme is to generate dimension-wise activation map for multivariate time series. To generate a dCAM you just have to upload the data and be sure an admin deployed your model !</p>

                <div style={{'display':'flex','flex-direction':'row'}}>
                        
                        <h1>1. Project name :</h1>
                        <Input  placeholder="Project Name" value={selectedOption} onChange={(e) => setSelectedOption(e.target.value)} style={{'height':'20%','width':'30%','margin':'10px',marginLeft:'100px'}}  />
                    </div>
                <div style={{'display':'flex','flex-direction':'row'}}>
                    <h1>2. Upload Data : </h1>
                    <Upload {...uploadProps}>
                        <Button style={{'marginLeft':'50px'}} icon={<UploadOutlined />}>Select a zip file</Button>
                    </Upload>
                    <p style={{'marginLeft':'10px','font-size':'15px','marginTop':'10px'}}>(.zip)</p>
                </div>
                
                <Button
                    type="primary"
                    onClick={handleUpload}
                    disabled={fileList.length === 0}
                    style={{ marginTop: 16 ,backgroundColor: '#112d4e',width: '200px'}}   
                    >
                <p style={{color: 'white'}}>Upload</p>
                </Button>
                <p style={{'font-size': '25px', 'text-align': 'left', 'margin': '20px','marginBottom':'70px'}}>Train your own model !</p>
                <Button
                    type="primary"
                    onClick={redirect => window.location.href='https://gitlab.com/Masi-s-Sebdeb/tfe/-/tree/main/Template'}
                    style={{ marginTop: 16 ,backgroundColor: '#112d4e',width: '200px'}}   
                    >
                <p style={{color: 'white'}}>Download Jupyter</p>
                </Button>

            </div>
            <div style={{"width":"50%","marginLeft":'100px','marginTop':'20px'}}>
                <div>
                {options.map((project) => (
                    <div style={{'display':'flex','flex-direction':'row'}}>
                        <h1>{project}</h1>
                        <Button type="primary" danger onClick={() => handleDelete(project)} style={{'marginLeft':'50px'}}>Delete</Button>
                    </div>
                ))}
                </div>
            </div>
        </div>
    )
}

export default Admin
