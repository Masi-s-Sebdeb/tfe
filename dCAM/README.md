
from :

@article{dcam,
  author    = {Paul Boniol and Mohammed Meftah and Emmanuel Remy and
               Themis Palpanas},
  title     = {dCAM: Dimension-wise Class Activation Map for Explaining Multivariate Data Series Classification},
  journal   = {{Proceedings of the 2022 International Conference on Management of Data (SIGMOD'22)}},
  year      = {2022}
}

adapted to Tensorflow