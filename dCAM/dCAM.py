import random
import numpy as np
import tensorflow as tf
from tqdm import tqdm_notebook as tqdm

class DCAM():
	def __init__(self,model,last_conv_layer='layer3'):
		self.last_conv_layer = last_conv_layer
		self.model = model
		
	def run(self,instance,nb_permutation,label_instance):
		all_permut,permut_success = self.__compute_permutations(instance, nb_permutation,label_instance)
		dcam =  self.__extract_dcam(self.__merge_permutation(all_permut))
		return dcam,permut_success

	def __gen_cube_random(self,instance):
		result = []
		result_comb = []
		initial_comb = list(range(len(instance)))
		random.shuffle(initial_comb)
		for i in range(len(instance)):
			result.append([instance[initial_comb[(i+j)%len(instance)]] for j in range(len(instance))])
			result_comb.append([initial_comb[(i+j)%len(instance)] for j in range(len(instance))]) 
		
		return result,result_comb


	def __merge_permutation(self,all_matfull_list):
		full_mat_avg = np.zeros((len(all_matfull_list[0]),len(all_matfull_list[0][0]),len(all_matfull_list[0][0][0])))
		for i in range(len(all_matfull_list[0])):
			for j in range(len(all_matfull_list[0][0])):
				mean_line = np.array([np.mean([all_matfull_list[k][i][j][n] for k in range(len(all_matfull_list))]) for n in range(len(all_matfull_list[0][0][0]))])
				full_mat_avg[i][j] = mean_line
		return full_mat_avg

	def __extract_dcam(self,full_mat_avg):
		return np.mean((full_mat_avg-np.mean(full_mat_avg,1))**2,1)*np.mean(np.mean(full_mat_avg,1),0)

	def __make_gradcam_heatmap(self,img_array, pred_index=None):
    
		img_array = np.array(img_array)
		img_array = img_array.reshape((1,)+img_array.shape)
		img_array = np.transpose(img_array, (0, 2, 3, 1))
		predict = self.model.predict(img_array)
		predict = np.argmax(predict)
		
		grad_model = tf.keras.models.Model(
			[self.model.inputs], [self.model.get_layer(self.last_conv_layer).output, self.model.output]
		)

		# Then, we compute the gradient of the top predicted class for our input image
		# with respect to the activations of the last conv layer
		with tf.GradientTape() as tape:
			last_conv_layer_output, preds = grad_model(img_array)
			if pred_index is None:
				pred_index = tf.argmax(preds[0])
			class_channel = preds[:, pred_index]

		# This is the gradient of the output neuron (top predicted or chosen)
		# with regard to the output feature map of the last conv layer
		grads = tape.gradient(class_channel, last_conv_layer_output)

		# This is a vector where each entry is the mean intensity of the gradient
		# over a specific feature map channel
		pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))

		# We multiply each channel in the feature map array
		# by "how important this channel is" with regard to the top predicted class
		# then sum all the channels to obtain the heatmap class activation
		last_conv_layer_output = last_conv_layer_output[0]
		heatmap = last_conv_layer_output @ pooled_grads[..., tf.newaxis]
		heatmap = tf.squeeze(heatmap)
		
		# For visualization purpose, we will also normalize the heatmap between 0 & 1
		#heatmap = tf.maximum(heatmap, 0) / tf.math.reduce_max(heatmap)
		return heatmap.numpy(),predict

	def __compute_multidim_cam(self,instance,nb_dim,index_perm):
		acl,comb = self.__gen_cube_random(instance)
		overlay,pred_class = self.__make_gradcam_heatmap(acl)
		full_mat = np.zeros((nb_dim,nb_dim,len(overlay[0])))
		for i in range(nb_dim):
			for j in range(nb_dim):
				full_mat[comb[i][j]][i] = overlay[j]
		return overlay,full_mat,pred_class


	def __compute_permutations(self,instance, nb_permutation,label_instance):
		all_pred_class = []
		all_matfull_list = []

		final_mat = np.zeros((len(instance),len(instance)))
		for k in tqdm(range(0,nb_permutation)):
			_,fmat,class_pred = self.__compute_multidim_cam(instance,len(instance),k)
			if class_pred == label_instance :
				all_matfull_list.append(fmat)
			all_pred_class.append(class_pred)
		if np.std(all_pred_class) == 0:
			return all_matfull_list,nb_permutation
		else:
			#verbose
			#print("[WARNING]: misclassification for some permutations:")
			#print("|--> Please note that every misclassified permutations will not be taken into account")
			#print("|--> The total number of permutation used to compute DCAM is lower than the number given as parameter")
			#print("|--> Number correctly classified permutations: {}".format(all_pred_class.count(label_instance)))
			#print("|--> Percentage of correctly classified permutations: {}".format(float(all_pred_class.count(label_instance))/float(len(all_pred_class))))
			return all_matfull_list,float(all_pred_class.count(label_instance))