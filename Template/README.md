This folder contains sample files needed to create a project in the admin platform.

File upload format :

A zip file containing the following files:

- config_file.txt containing a comma-separated list of dimension names. Example: dimensions_list: dim1, dim2,...

- the various files concerning the model (more info in the jupyter notebook: ALL_STEP_DEPLOY.ipynb)

- possibly the files all_class_std_mean_0.npy and all_class_std_mean_0.npy, which will be used to generate the standard deviation graphs (see file std.ipynb).

The file should then be sent as a compressed file (.zip).

an exemple can be found in the the exemple.zip file